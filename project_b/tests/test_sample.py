from src import sample

def test_add():
    result = sample.add(1, 2)
    expect = 3

    assert result == expect

def test_substract():
    result = sample.substract(3, 1)
    expect = 2

    assert result == expect